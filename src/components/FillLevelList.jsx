import React, { Component } from "react";
import { Line } from "react-chartjs-2";
import VehicleFillLevel from "./VehicleFillLevel";

class FillLevelList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    };
  }

  render() {
    /*  let chartData = {
      datasets: [
        {
          label: this.props.data.Title,
          fillColor: "rgba(220,220,220,0.2)",
          strokeColor: "rgba(220,220,220,1)",
          pointColor: "rgba(220,220,220,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: this.state.data
        }
      ]
    };*/

    if (this.props.data) {
      return this.props.data.map((item, index) => (
        <VehicleFillLevel key={index} data={item} />
      ));
    } else {
      return <div>-</div>;
    }
  }
}

// <Line data={chartData} width="600" height="250" />
export default FillLevelList;
