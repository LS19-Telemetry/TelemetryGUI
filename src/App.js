import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import M from "materialize-css";

import Navigation from "./components/nav";
import Footer from "./components/footer";
import VehicleList from "./components/VehicleList";

class App extends Component {
  render() {
    return (
      <div>
        <Navigation />

        <div className="container">
          <VehicleList />
        </div>

        {/* <Footer /> */}
      </div>
    );
  }
}

export default App;
