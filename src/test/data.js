var TestData = {};
TestData = {
  XMLName: {
    Space: "",
    Local: "TelemetryData"
  },
  Vehicles: {
    Vehicle: [
      {
        IsActive: false,
        IsTrain: "true",
        HelperActive: "false",
        ViVehicleName: "nil",
        Name: "E - Locomotive",
        Desc: "Lokomotive",
        CruiseControl: {
          Status: "0",
          Speed: "80",
          SpeedSent: "80",
          IsActive: "true"
        },
        MotorStarted: "false",
        OnField: "false",
        Field: "0",
        Speed: "0",
        InWater: "false",
        Damaged: "false",
        Attachments: "4",
        Pos: {
          Z: "",
          X: ""
        },
        Blocked: "",
        Fill: {
          Level: "",
          Capacity: "",
          Percent: "",
          Inverted: "",
          Title: "",
          Name: "",
          ID: ""
        }
      },
      {
        IsActive: "false",
        IsTrain: "false",
        HelperActive: "true",
        ViVehicleName: "Massey Ferguson 7722",
        Name: "7722",
        Desc: "Traktor",
        CruiseControl: {
          Status: "1",
          Speed: "40",
          SpeedSent: "40",
          IsActive: "true"
        },
        MotorStarted: false,
        OnField: "true",
        Field: "7563",
        Speed: "12",
        InWater: "false",
        Damaged: "false",
        Attachments: "2",
        Pos: {
          Z: "-126.172295",
          X: "200.845871"
        },
        Blocked: "false",
        Fill: {
          Level: "",
          Capacity: "",
          Percent: "",
          Inverted: "",
          Title: "",
          Name: "",
          ID: ""
        }
      },
      {
        IsActive: "false",
        IsTrain: "false",
        HelperActive: "false",
        ViVehicleName: "Fendt Favorit 515c",
        Name: "Favorit 515c",
        Desc: "Traktor",
        CruiseControl: {
          Status: "0",
          Speed: "10",
          SpeedSent: "10",
          IsActive: "true"
        },
        MotorStarted: "true",
        OnField: "true",
        Field: "5163",
        Speed: "7",
        InWater: "false",
        Damaged: "false",
        Attachments: "2",
        Pos: {
          Z: "237.480606",
          X: "284.668884"
        },
        Blocked: "false",
        Fill: {
          Level: "",
          Capacity: "",
          Percent: "",
          Inverted: "",
          Title: "",
          Name: "",
          ID: ""
        }
      },
      {
        IsActive: "true",
        IsTrain: "false",
        HelperActive: "false",
        ViVehicleName: "Massey Ferguson MF Activa 7347 S",
        Name: "MF Activa 7347 S",
        Desc: "Drescher",
        CruiseControl: {
          Status: "0",
          Speed: "27",
          SpeedSent: "33",
          IsActive: "true"
        },
        MotorStarted: "false",
        OnField: "true",
        Field: "5123",
        Speed: "0",
        InWater: "false",
        Damaged: "false",
        Attachments: "1",
        Pos: {
          Z: "258.498535",
          X: "191.355133"
        },
        Blocked: "false",
        Fill: {
          Level: "",
          Capacity: "",
          Percent: "",
          Inverted: "",
          Title: "",
          Name: "",
          ID: ""
        }
      }
    ]
  },
  LastUpdate: "2018-12-09T16:47:45.0473528+01:00",
  Source:
    "C:\\Users\\fb\\Documents\\My Games\\FarmingSimulator2019\\mods\\TelemetryData.xml"
};

module.exports = TestData;
